function removeByName(scene, objectName="model3d") {
   var selectedObject = scene.getObjectByName(objectName);
   scene.remove( selectedObject );
   // asuming re-rendering will happen in the background
   // we dont have to re-render here
}

function addObject(scene, object, name="model3d"){

   // TODO: implement better object naming 
   // object.name = name;
   scene.add(object);
}

function controlsEnabled(controls, bol){
   controls.enabled = bol
   // call controls.update()
   //not necessary called in animate
}

function controlsEnabledToggle(controls){
   controls.enabled = !controls.enabled
   // call controls.update()
   //not necessary called in animate
}


const splitLines = str => str.split(/\r?\n/);

function loadFile(path, callback_func) {
   $.get(path, null, callback_func, 'text').fail(function() {
      alert("Are you sure that is GCODE?");
   });
}



function standarize_gcode_line(gcode_line){
   if(!gcode_line.includes(";TYPE:")){
      gcode_line = gcode_line.replace(/;.*$/, '').trim(); // remove comments
   }
   gcode_line = gcode_line.toUpperCase();
   return gcode_line;
}

function split_gcode(gcode){
   lines = splitLines(gcode);
   return lines;
}

function toAbdolute(old, new_val, rel){
   // convert value to absolute
   return rel ? old + new_val : new_val;
}

function calculate_next_point(status, tokens){
   // calculate next point in absolute, using abs rel (e) settings
   // last point in absolute coordinates
   // new_coords w/ e in either abs or rel, with rel_e or abs_e
   // either do current_pos+new(rel) or return new
   // but if cord not in the new command then we leave the old point, this makes 
   //  new point fully defined

   last_point = status.abs_current_pos;
   new_coords = tokens;
   rel = status.realtive;
   rel_e = status.realtive_extrud;

   new_point = {
      x: new_coords.x !== undefined ? toAbdolute(last_point.x, new_coords.x, rel) : last_point.x,
      y: new_coords.y !== undefined ? toAbdolute(last_point.y, new_coords.y, rel) : last_point.y,
      z: new_coords.z !== undefined ? toAbdolute(last_point.z, new_coords.z, rel) : last_point.z,
      e: new_coords.e !== undefined ? toAbdolute(last_point.e, new_coords.e, rel_e) : last_point.e,

   };

   new_point.net_extruded = new_point.e - last_point.e; 
   // if no extrution happened then it resolves to 0. coolio 

   return new_point;
}



function Printer(build_volume){

   const geometry = new THREE.BoxGeometry( build_volume.x, build_volume.y, build_volume.z );

   const material = new THREE.MeshBasicMaterial( {color: 0x00ff00} );
   // const cube = new THREE.Mesh( geometry, material );
   // scene.add( cube )


   const wireframe = new THREE.WireframeGeometry( geometry );

   const printer = new THREE.LineSegments( wireframe );
   printer.material.depthTest = false;
   printer.material.opacity = 0.25;
   printer.material.transparent = true;

   // printer.position.x = build_volume.x/2;
   // printer.position.y = build_volume.y/2;
   printer.position.z = build_volume.z/2;
   return printer;
}




function createScene(element, DEBUG=false){
  
   const renderer = new THREE.WebGLRenderer();
   // renderer.setPixelRatio( window.devicePixelRatio );
   renderer.setSize( element.width(), element.height() );//call this after seting pixels
   element.append( renderer.domElement );

   renderer.clear();


   const scene = new THREE.Scene();
   const camera = new THREE.PerspectiveCamera( 75, element.width() / element.height(), 0.1, 1000 );
   camera.up = new THREE.Vector3( 0, 0, 1 ); //swap camara, as in cam y is z

   // TODO: Key binding events won't fire, FIX
   //       related to BUG
   //       https://github.com/mrdoob/three.js/issues/20805
   controls = new THREE.OrbitControls(camera, renderer.domElement);
   // controls = new THREE.TrackballControls(camera,renderer.domElement);


  
   if (DEBUG === true) {
      // alert("debuging enabled!!")
      // const lightHelper = new THREE.PointLightHelper(pointLight);
      // scene.add(lightHelper)

      const gridHelper = new THREE.GridHelper();

      scene.add(gridHelper);

   }


   camera.position.x = 0.1;
   camera.position.y = 5;
   camera.position.z = 6;

   camera.position.x = 0.1;
   camera.position.y = -250;
   camera.position.z = 150;


   // if a resize happens update stuff accordingly
   $(window).on('resize', function() {
      // needed when using TrackballControls, but we are not
      // controls.screen.width = window.innerWidth;
      // controls.screen.height = window.innerHeight;

      renderer.setSize(element.width(), element.height());
      
      camera.aspect = element.width() / element.height();
      camera.updateProjectionMatrix();
      
   });

   // from https://github.com/mrdoob/stats.js/
   var stats = new Stats();
   stats.showPanel( 1 ); // 0: fps, 1: ms, 2: mb, 3+: custom
   $(document.body).append( stats.domElement );

   function customRender(){
      stats.begin();
   
      renderer.render( scene, camera );

      stats.end();
   }
   // requestAnimationFrame(customRender);
   // controls.addEventListener( 'change', () => requestAnimationFrame(customRender) );


   function animate() {
      requestAnimationFrame(animate)
      // cube.rotation.x += 0.01;
      // cube.rotation.y += 0.01;

      stats.begin();

      controls.update();

      renderer.render( scene, camera );// dont render here, only when we move cam
   
      stats.end();

   }
   // renderer.render( scene, camera ); //render once since we are only doing when we move
   animate();



   return [scene,camera,controls];
}



$( document ).ready(function() {
   build_volume = {x:250,y:250,z:250};

   // alert( 'ready!' );
   scene_cam_con = createScene($("#renderLocation"),false);
   scene = scene_cam_con[0]; 
   camera = scene_cam_con[1];
   controls = scene_cam_con[2];

   printer = Printer(build_volume);
   // scene.add(printer);
   axesHelper = new THREE.AxesHelper( 50 );
   axesHelper.position.x = -build_volume.x/2;
   axesHelper.position.y = -build_volume.y/2;
   scene.add( axesHelper )

   colors = [ // TODO: automate this when using octoprint
      $('#color0').val(), //default aka no collor aplied
      $('#color1').val(),
      $('#color2').val(),
      $('#color3').val(),
      $('#color4').val(),
      $('#color5').val() // movement color
   ]
   
   path = '/cube.gcode'
   loadFile(path, function(gcode) {
      my_object = new GcodeObject(scene,gcode,0.2,0.4, colors);
      my_object.add_to_scene();
   });
   
   const mouse = new THREE.Vector2();
   function onMouseMove( event ) {

      // calculate mouse position in normalized device coordinates
      // (-1 to +1) for both components
      mouse.x = ( event.clientX / $("#renderLocation").width() ) * 2 - 1;
      mouse.y = - ( event.clientY / $("#renderLocation").height() ) * 2 + 1;
      // console.log("updating mouse", mouse.x,mouse.y)


   }
   window.addEventListener( 'mousemove', onMouseMove, false );
   window.addEventListener( 'mousedown', function(){mouse_down=true}, false );
   window.addEventListener( 'mouseup', function(){mouse_down=false}, false );

   const raycaster = new THREE.Raycaster();
   mouse_down = false; // can be used to tell if we are holding!
   

   function paint(){
      // calculate objects intersecting the picking ray

      if ($("#paint_toggle").prop('checked') && mouse_down){
         console.log("cliking paint")
         // update the picking ray with the camera and mouse position
         raycaster.setFromCamera( mouse, camera );

         my_object.links.forEach(function(link){
            // console.log(link.object)
            intersects = raycaster.intersectObject( link.object );
            if (intersects.length > 0){
               // console.log(intersects);

               link.change_color(parseInt($('#paint_color').find(":selected").val() ));
            }
            

         });
         
      }
      // else{
      //    console.log(".")
      // }
   }
   setInterval(paint, 0)
   // window.addEventListener( 'click', paint, false );


   // controlsEnabled(controls,false)
});

// TODO: important !! remove objects when cahngin material or geomtry because they are cached
//          https://stackoverflow.com/a/37009330/12044480

// TODO: when we create we use some weight values like thickness but when we rerender we dont
// TODO: add color for negatively extruded
// TODO: add flags for showing movements or not

// TODO: make simpler using sugar Lib
// TODO: make number of colors dynamic, and eveything related to that
//       like  color change handlers and gcode objects
// TODO: add stats panel optional/dev https://sbcode.net/threejs/stats-panel/

// TODO: integrate as octoprint pluging but as option because we also want standalone
// TODO: we always assume relative E !!!! so no support for M82 M83
// TODO: add alert that code in inches is not suported (G20) !!!! and quit

// TODO: support vase mode
// TODO: make gcode ignore list settable by octoprint plugin setings

// TODO: add loagin aniumation https://discourse.threejs.org/t/display-progress-bar-till-3d-model-gets-loaded-fully/699
// IDEAS:
//       we can gen color using this tool then we can send this color info to 
//       the python scrypt that controls hardware
//       that script can also send specific color modes
//    have an interface to controll hardware and color modes, or go to this tool to choose
//       colors

//       slicer    thisApp
// 3dmodel --> gcode(octoprint) --> color.file --> controller.py --> servos
//                          user -->color.file-/     |
//                            |  modes(eg rainbow)--/
//                             \__________/
//                                   |
//                                Plugin GUI



// usage
// make sure to say relative or absolute before printing
// and to home
// and to say absolute relative E


// warnings
// vase mode not suported!!!




// --------- notes --------
//
// how to interact with objects in three
// https://stackoverflow.com/questions/32087007/how-can-i-interact-with-objects-in-the-scene-in-three-js
// check out https://threejs.org/docs/#api/en/core/Raycaster
//
//
//
//
// debugging : https://discoverthreejs.com/tips-and-tricks/
//
// https://threejsfundamentals.org/threejs/lessons/threejs-optimize-lots-of-objects.html
//
//
// optimization: https://stackoverflow.com/questions/25219352/webgl-scene-doest-render-because-of-lost-context
// https://stackoverflow.com/questions/31407778/display-scene-at-lower-resolution-in-three-js
//
// optimize, split thing into chunks splited usin layer numbers in the file, so basicly do it insections

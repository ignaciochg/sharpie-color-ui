

function tokenize_gcode_line(gcode_line){
   gcode_line = standarize_gcode_line(gcode_line);
   parsed_line = {};
   if(gcode_line){ //if not comment
      tokens = gcode_line.split(' ');
      // console.log(tokens)
      args = {"flags":[]};


      parsed_line["command"] = tokens[0].toUpperCase();
      parsed_line["raw"] = gcode_line;
      parsed_line["tokens"] = tokens;

      if(gcode_line.includes(";TYPE:")){ // pseudo command for mesh types for optomization
         parsed_line["command"] = "MESH_TYPE";
         parsed_line["tokens"] = gcode_line.split(':');
         parsed_line["type"] = parsed_line["tokens"][1];
         return parsed_line;
      }

      tokens.splice(1).forEach(function(arg) { //optimize
         temp_args = {"flags":[]};
         if(arg.length == 1){ //support for flag arg in gcode line
            // console.log("flag ",arg)
            args["flags"].push(arg);
         }
         else{// if its an arg like E100
            
            var key = arg[0].toLowerCase(); // makes its easier on the eye in the code :)
            var value = parseFloat(arg.substring(1));
            // console.log("key: ", key, " val: ", value)
            args[key] = value;
         }
      });
      // console.log(args)



      return Object.assign(parsed_line,args);
   }
   else{
      return undefined;
   }   
}

function parse_gcode(gcode){
   lines = split_gcode(gcode);

   default_home = {"x":0,"y":0,"z":0} // TODO: octoprints set home here
   
   links = []; // should have x,y,z,layer#,extrude (from prev p to this one)
   var status = {
      "realtive_extrud": false,
      // "current_layer": 0,// get current layer off Z intead becasue what if first layer is diff?
      "abs_current_pos": {x:default_home.x, y: default_home.y, z:default_home.z, e: 0}, // assume we are homed when we start
      "realtive": false, //false=absolute, true=relative,
      "mesh_type": "idk",
      // "last_ext": , //is to be deducted from next strud if we had neg extrud in past

      // "layer_height": layer_height,
      // "nozzle_size": nozzle_size
   };

   function homeIt(){
      status.abs_current_pos.x = default_home.x;
      status.abs_current_pos.y = default_home.y;
      status.abs_current_pos.x = default_home.z;
   }
   // console.log(status);

   // function newLine(p1,p2,l,e){
   //    return {"p1":p1,"p2":p2, "layer":l, "extrude_length":e };
   // }
   // function newPoint(x,y,z,e){
   //    return {"x":x,"y":y, "z":z, "e":e }; // e is absolute !!!!
   // }
   for (const line of lines) {
      tokenized_line = tokenize_gcode_line(line);
      if(tokenized_line){ // skip empty/comment lines
         switch(tokenized_line.command){
            case "MESH_TYPE":
               status.mesh_type = tokenized_line.type;
               break;
            case "G28": // home
               homeIt(); // this doesnt change e
               break;
            
            case "G90": // set absolute positioning
               status.realtive = false;
               break;
            case "G91": // set relative positioning
               status.realtive = true;
               break;

            case "M82": // set absolute E
               status.realtive_extrud = false;
               break;
            case "M83": // set relative E
               status.realtive_extrud = true;
               break;

            case "G92": //set position, no movement or e
               // console.log(tokenized_line);
               // console.log(tokenized_line.x);
               // console.log(status.abs_current_pos.x);
               status.abs_current_pos = {
                  x: tokenized_line.x !== undefined ? tokenized_line.x : status.abs_current_pos.x,
                  y: tokenized_line.y !== undefined ? tokenized_line.y : status.abs_current_pos.y,
                  z: tokenized_line.z !== undefined ? tokenized_line.z : status.abs_current_pos.z,
                  e: tokenized_line.e !== undefined ? tokenized_line.e : status.abs_current_pos.e

               };
               break;

            case "G0": //move without extrude (but its optional), alias to G1
            case "G1":
               new_point = calculate_next_point(status, tokenized_line);
               
               extruded = new_point.net_extruded; 
               delete new_point.net_extruded; // pull it out and make it gone

               link_points = [status.abs_current_pos, new_point];

               // WARNING: if load times becomes too slow create link object here intead
               new_link = {points:link_points, extruded: extruded, tokenized:tokenized_line, mesh_type:status.mesh_type};
               // console.log(new_link);
               links.push(new_link);

               status.abs_current_pos = new_point;
               break;
            
            default:

               ignored_gcodes = [
                  "G2","G3","G5", // arc/spline https://community.ultimaker.com/topic/30133-does-cura-generate-g2-g3-or-g5-commands/
                  "G29", // bed level
                  "G4", "M400",// dwell/wait based on time or empty queue
                  "M104", "M140", // set hot-end/bed temps
                  "M105", //report temps to host asap
                  "M106","M107", // set fan speed, fan off
                  "M108", "M109", "M190",// break and continue, set-wait on extr/bed temp
                  "M84", "M18", // stop idle hold, disable steppers (reprap vs marlin fw)
                  "M500" // save settings to EEPROM
               ];

               if(ignored_gcodes.includes(tokenized_line.command)){
                  continue;
               }
               if (tokenized_line["command"].includes("M")) { // skip M commands
                  continue;
               }
               //else warn about not supported command
               console.log("Unrecognized Gcode: "+tokenized_line.raw.trim()+".");
               continue;

         }

      }
   }

   return links;

}


class GcodeObject {
   constructor(scene, gcode, layer_height, nozzle_size, colors) {
      this.gcode = gcode;
      this.scene = scene
      this.links = [];
      this.links_raw = [];
      this.colors = colors;
      this.nozzle_size = nozzle_size;
      this.layer_height = layer_height;
      this.object = new THREE.Object3D();

      this.parse_gcode();
   }

   parse_gcode(){
      // this.links.push(new GcodeLink(//todo definition changed)); // this line works, good for debuging
      this.links_raw = parse_gcode(this.gcode);

      // for (const link_info of this.links_raw){
      //    let visible = link_info.extruded == 0 ? false : true ;
      //    let color_numb = visible ? 0 : this.colors.length -1;
      //    new_link = new GcodeLink(link_info.tokenized, this.colors, color_numb, link_info.points,link_info.extruded );
      //    this.object.add(new_link.object);
      //    this.links.push(new_link);
      // }

      this.links_raw.forEach(function(link_info){ 
         let visible = link_info.extruded == 0 ? false : true ;
         if(!visible){//optimization, dont add moves
            return;
         }
         if(link_info.mesh_type == "FILL"){ // remove infill
            return; // we should still create link without threejs object for optimization, apply default fill color
         }
         let color_numb = visible ? 0 : this.colors.length -1;
         new_link = new GcodeLink(link_info.tokenized, this.colors, color_numb, link_info.points,link_info.extruded );
         this.object.add(new_link.object);
         this.links.push(new_link);
      }, this);// make this this inside
   }

   add_to_scene(){ // call this after adding links
      // for (const link of this.links) {//deprecated
      //    link.add_to_scene(this.scene);
      // }
      addObject(this.scene,this.object)
   }

   paint_all(color_number){
      console.log("paiting all with ", color_number)
      console.log(this.colors)
      this.links.forEach(function(link){
         link.change_color(color_number);
         link.change_color(color_number); // idk why but it fixes no tpainting the first time

      });
   }

   refresh_colors(colors){
      this.colors = colors
      this.links.forEach(function(link){
         // console.log(this.colors)
         link.update_colors(this.colors)
         link.change_color(link.color_numb);
      },this);
   }

}


class GcodeLink {
   constructor(tokenized_gcode, colors, color_numb, points, net_extruded) {
      this.tokenized_gcode = tokenized_gcode;
      this.color_numb = color_numb;
      this.colors = colors;
      this.points = points;
      this.points_vecs = [];
      for (const p of this.points){
        this.points_vecs.push( new THREE.Vector3( p.x, p.y, p.z) );
      }
      this.extruded = net_extruded;
      this.visible = net_extruded == 0 ? false : true ;

      // if (this.visible){
      this.construct_object();
      // }
      
   }

   construct_object(){
      // this.geometry = new THREE.BoxGeometry();
      // this.material = new THREE.MeshBasicMaterial( { color: this.colors[this.color_numb] } );
      // // const cube = new THREE.Mesh( geometry, material );
      // this.object = new THREE.Mesh( this.geometry, this.material );




      this.material = new THREE.LineBasicMaterial({
         color: this.visible ? this.colors[this.color_numb] : this.colors[this.colors.length -1] ,
         linewidth: 1, // ignored by windows !!!!
         linecap: 'round', //ignored by WebGLRenderer
         linejoin:  'round' //ignored by WebGLRenderer

      });
// this.visible ? 0x09F137 : this.colors[color_number];
      // const points = [];
      // points.push( new THREE.Vector3( - 10, 0, 0 ) );
      // points.push( new THREE.Vector3( 0, 10, 0 ) );
      // points.push( new THREE.Vector3( 10, 0, 0 ) );

      this.geometry = new THREE.BufferGeometry().setFromPoints( this.points_vecs );

      this.object = new THREE.Line( this.geometry, this.material );
      // console.log(this.object)
   }

   // add_to_scene(scene){ //deprecated
   //    // if(this.visible){
   //       // console.log(this.object)
   //    addObject(scene, this.object);   
   //    // }
      
   // }

   update_colors(colors){
      this.colors = colors;
   }
   change_color(color_number){
      // console.log(colors)
      // console.log(color_number)
      // console.log(this.colors[color_number])
      // this.material = new THREE.MeshBasicMaterial({ 
      //    color: this.visible ? this.colors[this.color_numb]: this.colors[this.colors.length -1] 
      // });
      // // console.log(this.object)
      // this.object.material = this.material;

      // console.log(color_number)
      this.material.color.set(this.visible ? this.colors[this.color_numb]: this.colors[this.colors.length -1]);
      this.color_numb = color_number;
   }

   // should not be needed
   // refresh_greometry(){ //call after changing points (array[vectors])
   //    this.geometry = new THREE.BufferGeometry().setFromPoints( this.points );
   //    this.object.geometry = this.geometry;
   // }
}

// docs
// why add this.color to each link?
// because we can jsut change the color-number witohut haiving to pass colors in eveytnime, easier now
// this uses too much space? probably as gcode grows but if it does we can change later